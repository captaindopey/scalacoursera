package stackoverflow

import org.scalatest.{FunSuite, BeforeAndAfterAll}
import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner
import org.apache.spark.SparkConf
import org.apache.spark.SparkContext
import org.apache.spark.SparkContext._
import org.apache.spark.rdd.RDD
import java.io.File

@RunWith(classOf[JUnitRunner])
class StackOverflowSuite extends FunSuite with BeforeAndAfterAll {


  lazy val testObject = new StackOverflow {
    override val langs =
      List(
        "JavaScript", "Java", "PHP", "Python", "C#", "C++", "Ruby", "CSS",
        "Objective-C", "Perl", "Scala", "Haskell", "MATLAB", "Clojure", "Groovy")

    override def langSpread = 50000

    override def kmeansKernels = 45

    override def kmeansEta: Double = 20.0D

    override def kmeansMaxIterations = 120
  }

  test("testObject can be instantiated") {
    val instantiatable = try {
      testObject
      true
    } catch {
      case _: Throwable => false
    }
    assert(instantiatable, "Can't instantiate a StackOverflow object")
  }

  test("Asdsad") {
    @transient lazy val conf: SparkConf = new SparkConf().setMaster("local").setAppName("StackOverflow")
    @transient lazy val sc: SparkContext = new SparkContext(conf)



    val means  = Array(
    (174995,174995)
    ,(174995,174995)
    ,(174995,174995)
    ,(174994,174995)
    ,(174994,174995)
    ,(174994,174995)
    ,(174995,174995)
    ,(174995,174995)
    ,(174995,174995)
    ,(174995,174995)
    )

    val vectors = List(
      (50000,25)
      ,(100000,1)
      ,(250000,11)
      ,(150000,11)
      ,(0,3)
      ,(50000,2)
      ,(100000,2)
      ,(0,2)
      ,(0,2)
      ,(100000,2)

    )

    val res = testObject.clusterResults(means, sc.parallelize(vectors))
    testObject.printResults(res)
  }


}