package calculator

object Polynomial {
  def computeDelta(a: Signal[Double], b: Signal[Double],
      c: Signal[Double]): Signal[Double] = {
    Signal(math.pow(b(), 2) - 4  * a() * c())
  }

  def computeSolutions(a: Signal[Double], b: Signal[Double],
      c: Signal[Double], delta: Signal[Double]): Signal[Set[Double]] = {
//    (-b ± √Δ) / 2a

    Signal({
        val deltaVal = delta()

        if (deltaVal < 0)
          Set()
        else {
          val bVal = b()
          val aVal = a()

          val first = (-bVal + math.sqrt(deltaVal)) / (2 * aVal)

          if (deltaVal == 0)
            Set(first)
          else {
            val second = (-bVal - math.sqrt(deltaVal)) / (2 * aVal)
            Set(first, second)
          }
        }
    })
  }
}
