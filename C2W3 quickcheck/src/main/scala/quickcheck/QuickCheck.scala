package quickcheck

import common._

import org.scalacheck._
import Arbitrary._
import Gen._
import Prop._

abstract class QuickCheckHeap extends Properties("Heap") with IntHeap {

  lazy val genHeap: Gen[H] = oneOf(const(empty), for {
    int <- arbitrary[Int]
    heap <- genHeap
  } yield insert(int, heap))

  implicit lazy val arbHeap: Arbitrary[H] = Arbitrary(genHeap)

  property("findMin inserting and finding min should find the same element") = forAll { a: Int =>
    val h = insert(a, empty)
    findMin(h) == a
  }

  property("findMin after inserting two elements should return smallest val") = forAll { (a: Int , b: Int )=>
    val min =  if (a < b) a else b
    val max =  if (a > b) b else a

    val h1 = insert(min, empty)
    val h2 = insert(max, empty)
    findMin(h2) == min
  }

  property("inserting the minimum again into a heap should return the minimum") = forAll { (h: H) =>
    val m = if (isEmpty(h)) 0 else findMin(h)
    findMin(insert(m, h)) == m
  }

  property("should get a sorted sequence of elements when continually finding and deleting minima") = forAll { (h: H) =>
    def check(heap: H, minList: List[Int]): List[Int] = {
      if (isEmpty(heap)) {
        minList
      } else {
        val min = findMin(heap)
        val newHeap = deleteMin(heap)
        check(newHeap, minList :+ min)
      }
    }

    val heapList = check(h, List())
    val sortedList = heapList.sorted
    sortedList == heapList
  }

  property("Finding a minimum of the melding of any two heaps should return a minimum of one or the other.") = forAll { (h1: H, h2: H) =>
    val m1 = if (isEmpty(h1)) 0 else findMin(h1)
    val m2 = if (isEmpty(h2)) 0 else findMin(h2)
    val newHeap = meld(h1, h2)

    val newMin = if (isEmpty(newHeap)) 0 else findMin(newHeap)

    newMin == m1 || newMin == m2
  }

  property("After adding a larger Int then smaller Int, the findMin after calling deleteMin should return the larger number") = forAll { a: Int =>
    val h1 = insert(2, empty)
    val h2 = insert(1, h1)

    val res = deleteMin(h2)
    val min = findMin(res)
    min == 2
  }


  property("Adding incrementally larger numbers (3 or more times) and deleting and finding min should return the second number added") = forAll { a: Int =>
    val h1 = insert(1, empty)
    val h2 = insert(2, h1)
    val h3 = insert(3, h2)

    val res = deleteMin(h3)
    val min = findMin(res)
    min == 2
  }

  }
