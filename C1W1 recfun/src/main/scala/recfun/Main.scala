package recfun

object Main {
  def main(args: Array[String]) {
    println("Pascal's Triangle")
    for (row <- 0 to 10) {
      for (col <- 0 to row)
        print(pascal(col, row) + " ")
      println()
    }
  }

  /**
   * Exercise 1
   */
    def pascal(c: Int, r: Int): Int = {
        if (c <= 0 || c >= r)
          return 1

       pascal(c - 1, r - 1) + pascal(c, r - 1)
    }
  
  /**
   * Exercise 2
   */
    def balance(chars: List[Char]): Boolean = {

      def counter(count: Int, subChar: List[Char]): Boolean = {
        if (count < 0)
            return false

        if (subChar.isEmpty)
            return count == 0

        val thisChar = subChar.head

        if (thisChar == '(')
          return counter(count + 1, subChar.tail)

          if (thisChar == ')')
            return counter(count - 1, subChar.tail)

        counter(count, subChar.tail)
      }
      counter(0, chars)
    }
  
  /**
   * Exercise 3
   */
    def countChange(money: Int, coins: List[Int]): Int = {
      if (coins.isEmpty)
        return 0

      def accumulator(total: Int, focus: Int, remainder: List[Int]): Int = {
        if (remainder.isEmpty) {
          if (total % focus == 0)
            return 1
          else
            return 0
        }

        val subTotal = total - focus

        val adjacentTally = accumulator(total, remainder.head, remainder.tail)

        if (subTotal > 0)
          return accumulator(subTotal, focus, remainder) + adjacentTally

        if (subTotal == 0)
            return 1 + adjacentTally

          adjacentTally
      }

      accumulator(money, coins.head, coins.tail)
    }
  }
