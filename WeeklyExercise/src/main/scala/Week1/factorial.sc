def factorial(n: Int): Int = {

  def counter(accumulation: Int, current: Int): Int =
    if (current == 0) return accumulation
    else counter(accumulation * current, current -1)

  counter(1, n)
}


factorial(5)