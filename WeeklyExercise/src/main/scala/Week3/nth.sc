//def nth[T](n: Integer, list: List[T]): T = {
//  if (n < 0)
//    throw new IndexOutOfBoundsException("cannot be negative")
//  else if (n >= list.length)
//    throw new IndexOutOfBoundsException("cannot be larger than the list size")
//  else
//    list.slice(n, 1).head
//}


def nth[T](n: Integer, list: List[T]): T =
    if (list.isEmpty)
        throw new IndexOutOfBoundsException()
    else
        nth(n - 1, list.tail)
