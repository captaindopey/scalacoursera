

trait Expr

case class Number(n: Int) extends  Expr
case class Sum(e1: Expr, e2: Expr) extends Expr
case class Var(x: String) extends Expr
case class Prod(e1: Expr, e2: Expr) extends Expr


def show(e: Expr): String = e match {
  case Number(n) => n.toString
  case Sum(l, r) => {
    if (l.isInstanceOf[Prod])
      "(" + show(l) + ") + " + show(r)
    else
      show(l) + " + " + show(r)
  }
  case Prod(l, r) => show(l) + " * " + show(r)
  case Var(x) => x.toString
}

show(Sum(Number(2), Number(44)))

show(Sum(Prod(Number(2), Var("x")), Var("y")))
show(Prod(Sum(Number(2), Var("x")), Var("y")))