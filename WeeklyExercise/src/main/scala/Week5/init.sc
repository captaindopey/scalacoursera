def init[T](xs: List[T]): List[T] = xs match {
  case List() => throw new Error("init of empty list")
  case List(x) => List()
  case y :: ys => y :: init(ys)
}


init(List(3, 2, 1))


val a = List(3, 2, 1)
val b = List(6, 5, 4)

a ::: b