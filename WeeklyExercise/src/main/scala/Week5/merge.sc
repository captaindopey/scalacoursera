def merge(xs: List[Int], ys: List[Int]): List[Int] = (xs, ys) match {
  case ( Nil, f ) => f
  case ( f, Nil ) => f
  case ( x1 :: x2, y1 :: y2) => if (x1 < y1) x1 :: merge(x2, ys) else y1 :: merge(xs, y2)
}



merge(List(2, 4, 6), List(1, 3, 5))

