def mapFun[T, U](xs: List[T], f: T => U): List[U] =
  (xs foldRight List[U]())((x, y) => f(x) :: y )

def lengthFun[T](xs: List[T]): Int =
  (xs foldRight 0)((x, y) => y + 1)


//lengthFun(List(1, 2, 3))
val a = List[Int](1, 2, 3)
mapFun[Int, Int](a, (x => x * 2))
