


def pack[T](xs: List[T]): List[List[T]] = xs match {
  case Nil => Nil
  case x :: xs1 => {
    val a = xs.span(p => p == x)
    val b = pack(a._2)
    // Should be just a :: b
    b match {
      case Nil => List(a._1)
      case y :: ys => a._1 :: b
    }

  }
}

pack(List("a", "a", "a", "b", "c", "c", "a"))

//List(List("a", "a", "a"), List("b"), List("c", "c"), List("a"))


def encode[T](xs: List[T]): List[(T, Int)] = pack(xs).map(x => ( x.head, x.length))

encode(List("a", "a", "a", "b", "c", "c", "a"))

