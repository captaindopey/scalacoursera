def removeAt[T](n: Int, xs: List[T]): List[T] = xs match {
  case List() => xs
  case List(x) => if (n == 0) List() else xs
  case y :: ys => if (n == 0) ys else y :: removeAt(n - 1, ys)
}



removeAt(1, List('a', 'b', 'c', 'd')) // List(a, c, d)